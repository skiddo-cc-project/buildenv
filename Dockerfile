FROM ubuntu:20.04

ENV \
	TERM=xterm-256color \
	DEBIAN_FRONTEND=noninteractive

RUN set -ex \
	&& apt-get -qq update \
	&& apt-get -qq install build-essential cmake gcovr python3-pip clang-format \
	&& apt-get autoclean && apt-get autoremove \
	&& apt-get -qq clean \
	&& rm -rf /var/lib/apt/lists/* \

	&& pip install cpplint \
	&& (pip cache purge || true) \
	&& rm -rf /root/.cache/*

# install build dependencies
RUN set -ex \
	&& apt-get -qq update \
	&& apt-get -qq install libreadline-dev \
	&& apt-get -qq clean \
	&& rm -rf /var/lib/apt/lists/*
